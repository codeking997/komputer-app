//this object takes in 5 different computer object, they store the type, the price, brand and an image of the computer

let noPreviousLoan = false;

let computer = {
     
    "computer1": {
       type: "HP 55",
       price: 5000,
       brand: "HP",
       features: "great computer, runs fast and kills aliens",
       image: "../images/hp.jpg"
   },

    "computer2": {
       type: "Mac 1000000",
       price: 10000,
       brand: "Apple",
       features: "very expensive and only good if you are a hipster, no gaming",
       image: "../images/images.jpg"
   },

    "computer3": {
       type: "Chromebook",
       price: 3000,
       brand: "Samsung",
       features: "let google know everything you do is not creepy at all",
       image: "../images/chromebook.jpg"
   },
    "computer4": {
       type: "Dell 50",
       price: 14000,
       brand: "Dell",
       features: "Dell bell",
       image: "../images/dell.jpg"
   },
    "computer5": {
       type: "vivobook",
       price: 20000,
       brand: "ASUS",
       features: "vivobook emobook",
       image: "../images/vivobook.jpg"
   }
}

//this function allows the user to request a loan
/*
Had to make a change and comment out noPreviousLoan and set it as a global variable 
otherwise it refused to be reset to true when the function was being run.
this method has a boolean that is set to false, this is to check that
the user does not have more than one loan. 
askLoan takes in a prompt which is parsed as an int so that the input is taken
in as an integer. userBalance calls the balance id in html
it is also parsed as an int, therefore we can compare it to askloan
the if statement first checks if there is a previous loan,
if false it goes to the second if which checks if the balance is greater
than zero if so then it goes to the third if which checks if the balance
is twice as big as the loan requested. the way I have done that it is by checking if the userBalance times 2 is smaller
than the loan input by the user.
if this is true you get a prompt message. otherwise the loan amount is added to the userbalance
and it is updated in the html

*/
function loanInputFunction(){
    
    //let noPreviousLoan = false;
    let askLoan = parseInt(prompt("Enter the amount you wish to borrow"));
    let userBalance = parseInt(document.getElementById('balanceId').innerHTML);

    if(noPreviousLoan==false){
        console.log("first noPreviousLoan statement", noPreviousLoan);
        if(userBalance>0){
            
            if(askLoan>userBalance*2){
                console.log("trying to get the amount:", askLoan*2);
                console.log(userBalance*2);
                alert("you cannot borrow this much");
                
            }else {
                
                askLoan+=userBalance;
                document.getElementById('balanceId').innerHTML = askLoan;
                console.log(askLoan); 
                noPreviousLoan=true;
                console.log("inside the else statement with no previous loan", noPreviousLoan);
               
                
            }
        }else{
            alert("you must have a deposite in the bank, you get a job!!!");
        }
        //noPreviousLoan=true;
        console.log("end of if with no previous loan", noPreviousLoan);
    }else if(noPreviousLoan==true){
        alert("you already have a loan");
    }
    console.log("very end:", noPreviousLoan);
    
    
}

//work method allows the user to work to increments the amount by 100 each time and then updates the html
function workInput(){
    let amount = 100;
    let foo = parseInt(document.getElementById('amountId').innerHTML);
    foo+=amount;
    document.getElementById('amountId').innerHTML = foo;
}

//deposite method
/*
this method allows the user to deposite money from their work amount to their bank
takes in a user input and parses it as an int. 
the if checks if the deposite is a number, is not null and is smaller than or equal to
the amount in amount work.
if so then it calls the balanceId
the amount is then added to the balance. 
the newAmount variables stores the amount in the work amount after the deposite has been made
the else if alerts that you don't have enough money to make the deposite
*/
function depositeInput(){
    
    let dep = parseInt(prompt("enter the amount you want to deposite"));
    let amountInWork = parseInt(document.getElementById('amountId').innerHTML);
    let newAmount = parseInt(document.getElementById('balanceId').innerHTML);
    if(dep!=null&&dep<=amountInWork){
        let yes = parseInt(document.getElementById('balanceId').innerHTML);
        yes += dep;
        document.getElementById('balanceId').innerHTML = yes;
        newAmount = amountInWork - dep;
        document.getElementById('amountId').innerHTML = newAmount;
    }else if(dep>=amountInWork){
        alert("you don't have enough money");
    }
    
}
/*
allows the user to buy a computer
the if checks if the balance is greater than or equal to
the price and if so then it alerts you that you have bought a new computer
and then deducts the amount from the balance and updates the html
the else tells you that the user needs more money to make the purchase.
*/
function buyFunction(){
    
    let balance = parseInt(document.getElementById('balanceId').innerHTML);
    let price = parseInt(document.getElementById("laptopPrice").innerHTML);
    const element = document.getElementById("computerId");
    const computerTypeBought = computer[element.value];

    if(balance>=price){
        alert(`you now own a brand new ${computerTypeBought.type}`);
        balance -= price;
        document.getElementById('balanceId').innerHTML = balance;
        
        
    }else{
        alert("you must work more you prick");
    }
    
    
    
/*
the select computer makes it possible to select a computer from the dropdown
and display the information about it. 
*/
    
}
function selectComputer(element){
    const yes = computer[element.value];
    //document.getElementById("featureComputer").innerHTML = yes.features;
    document.getElementById("featureComputer").innerHTML=  yes.features;
    document.getElementById("laptopName").innerHTML=  yes.type;
    document.getElementById("laptopPrice").innerHTML= yes.price;
    document.getElementById("laptopBrand").innerHTML= yes.brand;
    document.getElementById("compId").src  = yes.image;
    
    
}
/*
this method is loaded once the script is loaded
it takes a for loop that goes through all the objects in the 
computer object. it then dynamically creates an option element for each computer
it projects information from the objects onto the options elements. and then appends
it to the list 
*/
window.onload = function(){
    
    let computerSel = document.getElementById("computerId");
    for(let laptop in computer){
        const optionsElement = document.createElement("option");
        optionsElement.innerText = computer[laptop].type;
        optionsElement.value = laptop;
        computerSel.appendChild(optionsElement);
        console.log(optionsElement);
        console.log(computer[laptop].type, computer[laptop].price);
        
    }
   
}
